(function (views) {
    views.Items = Backbone.View.extend({
        tagName: 'ul',
        className: 'g-list',
        initialize: function () {
            _.bindAll (this, 'render', 'addAll', 'addOne');
            var self = this;
            self.collection.bind('reset', self.addAll);
            $(self.el).appendTo('#mainContent');
        },
        addAll: function () {
            var self = this;
            $(self.el).empty();
            self.collection.each(self.addOne);
        },
        addOne: function (model) {
            var self = this;
            var view = new views.Item({model:model});
            view.render();
            $(self.el).append(view.el);
        }
    });    
})(App.views);