(function (views) {
    views.Item = Backbone.View.extend({
        tagName: 'li',
        className: 'g-item',
        my_template: _.template($('#itemTemplate').html()),
        initialize: function() {
            this.render();
        },
        render: function () {
            this.$el.html(this.my_template(this.model.toJSON()));
        }
    });    
})(App.views);