(function (collections, model) {
    collections.Items = Backbone.PageableCollection.extend({
        url: "https://api.github.com/search/issues",
        model: model,
        state: {
            pageSize: 15,
            sortKey: "updated",
            order: 1
        },
        queryParams: {
            totalPages: null,
            totalRecords: null,
            sortKey: "sort",
            q: "state:closed repo:jashkenas/backbone"
        },
        parseState: function (resp, queryParams, state, options) {
            window.location.hash = '#' + state.currentPage;
            return {totalRecords: resp.total_count};
        },
        parseRecords: function (resp, options) {
            return resp.items;
        }
    });
})(App.collections, App.models.Item);